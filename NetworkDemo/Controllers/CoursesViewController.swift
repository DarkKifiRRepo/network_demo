//
//  CoursesViewController.swift
//  NetworkDemo
//
//  Created by Александр Евсеев on 25/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

class CoursesViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var courses: [CoursesModel] = [CoursesModel]()
    var methodType: SectionCollectionType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.separatorColor = #colorLiteral(red: 0.1831192612, green: 0.252234124, blue: 0.7498215419, alpha: 1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        switch methodType {
        case .httpsMethods?: fetchDataHTTPS()
        case .alamofireMethods?: fetchDataAlamofire()
        default: return
        }
    }
    
    private func fetchDataHTTPS() {
        NetworkManager.fetchCoursesData(with: listOfURLs.fetchCoursesURL) { [unowned self] courses in
            guard let courses = courses else { return }
            self.courses = courses
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    private func fetchDataAlamofire() {
        AlamofireManager.fetchCourses(with: listOfURLs.fetchCoursesURL) { [unowned self] courses in
            guard let courses = courses else { return }
            self.courses = courses
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    
}

extension CoursesViewController: UITableViewDelegate { }

extension CoursesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: listOfIdentifiers.tableCell, for: indexPath) as! CustomTableViewCell
        
        cell.configureCell(with: courses[indexPath.row])
        
        NetworkManager.downloadImage(with: courses[indexPath.row].imageUrl!) { image in
            DispatchQueue.main.async {
                cell.imageLogoView.image = image
            }
        }
        
        return cell
    }
}
