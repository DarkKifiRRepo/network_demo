//
//  ImageViewController.swift
//  NetworkDemo
//
//  Created by Александр Евсеев on 25/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var compeletedLabel: UILabel!
    
    var imageURL = listOfURLs.imageDownloadURL
    var methodType: SectionCollectionType!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        progressView.isHidden = true
        compeletedLabel.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        switch methodType {
        case .httpsMethods?: httpRequestMethod()
        case .alamofireMethods?: alamofireRequestMethod()
        default: return
        }
        
    }
}

extension ImageViewController {
    private func httpRequestMethod() {
        activityIndicator.startAnimating()
        NetworkManager.downloadImage(with: imageURL) { [unowned self] fetchImage in
            guard let fetchImage = fetchImage else { return }
            DispatchQueue.main.async {
                self.imageView.image = fetchImage
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    private func alamofireRequestMethod() {
        activityIndicator.startAnimating()
        
        AlamofireManager.onProgress = { progress in
            self.progressView.isHidden = false
            self.progressView.progress = Float(progress)
        }
        
        AlamofireManager.completed = { progress in
            self.compeletedLabel.isHidden = false
            self.compeletedLabel.text = progress
        }
        
        AlamofireManager.downloadImage(with: imageURL) { [unowned self] fetchImage in
            guard let fetchImage = fetchImage else { return }
            DispatchQueue.main.async {
                self.imageView.image = fetchImage
                self.activityIndicator.stopAnimating()
                self.progressView.isHidden = true
                self.compeletedLabel.isHidden = true
            }
        }
    }
}
