//
//  ViewController.swift
//  NetworkDemo
//
//  Created by Александр Евсеев on 25/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit
import UserNotifications

class MenuViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var alert: UIAlertController!
    private let backgroundTask = BackgroundManager()
    private var filePath: String?
    
    override func viewDidLoad() {
        collectionView.dataSource = self
        collectionView.delegate = self
        
        backgroundTask.fileLocation = { location in
            self.filePath = location.absoluteString
            self.alert.dismiss(animated: false, completion: nil)
            NotificationManager.postNotification(filePath: self.filePath!)
        }
    }
    
    private func showAlert() {
        
        alert = UIAlertController(title: "Downloading...", message: "0%", preferredStyle: .alert)
        let height = NSLayoutConstraint(item: alert.view!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: 170)
        alert.view.addConstraint(height)
        let cancelAction = UIAlertAction(title: "cancel", style: .destructive) { [unowned self] (action) in
            self.backgroundTask.stopDownload()
        }
        alert.addAction(cancelAction)
        present(alert, animated: true) {
            let size = CGSize(width: 40, height: 40)
            let point = CGPoint(x: self.alert.view.frame.width / 2 - size.width / 2,
                                y: self.alert.view.frame.height / 2 - size.height / 2)
            let activityIndicator = UIActivityIndicatorView(frame: CGRect(origin: point, size: size))
            activityIndicator.color = .gray
            activityIndicator.startAnimating()
            
            let progressView = UIProgressView(frame: CGRect(x: 0, y: self.alert.view.frame.height - 44, width: self.alert.view.frame.width, height: 2))
            progressView.tintColor = .blue
            
            self.backgroundTask.onProgress = { progress in
                progressView.progress = Float(progress)
                self.alert.message = String(Int(progress * 100)) + "%"
            }
            
            self.alert.view.addSubview(activityIndicator)
            self.alert.view.addSubview(progressView)
        }
    }
}

extension MenuViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? CustomCollectionViewCell else { return }
        
        switch cell.sectionType { // In performSegue we use SectionCollectionType for prepare and call method on next view controller
        case .httpsMethods?:
            switch cell.menuItem! {
            case .downloadImage: performSegue(withIdentifier: listOfSegues.segueImage, sender: cell.sectionType)
            case .getRequest: NetworkManager.getRequest(with: listOfURLs.getRequestURL)
            case .postRequest: NetworkManager.postRequest(with: listOfURLs.postRequestURL)
            case .coursesFetch: performSegue(withIdentifier: listOfSegues.segueTable, sender: cell.sectionType)
            case .downloadFile:
                NotificationManager.registerForNotification()
                showAlert()
                backgroundTask.startDownload(with: listOfURLs.download20Mb)
            }
        case .alamofireMethods?:
            switch cell.menuItem! {
            case .getRequest: AlamofireManager.sendRequest(with: listOfURLs.getRequestURL)
            case .postRequest: AlamofireManager.postRequest(with: listOfURLs.postRequestURL)
            case .coursesFetch: performSegue(withIdentifier: listOfSegues.segueTable, sender: cell.sectionType)
            case .downloadImage: performSegue(withIdentifier: listOfSegues.segueImage, sender: cell.sectionType)
            case .downloadFile: print("Dummy download file")
            }
        default: return
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        switch identifier {
        case listOfSegues.segueImage:
            guard let destination = segue.destination as? ImageViewController,
                let methodType = sender as? SectionCollectionType
                else { return }
            destination.methodType = methodType
        case listOfSegues.segueTable:
            guard let destination = segue.destination as? CoursesViewController,
                let methodType = sender as? SectionCollectionType
                else { return }
            destination.methodType = methodType
        default: return
        }
    }
}

extension MenuViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return MenuItems.allCases.count
        case 1:
            return MenuItems.allCases.count
        case 2:
            return 0
        default:
            return 0
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return SectionCollectionType.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: listOfIdentifiers.collectionCell, for: indexPath) as! CustomCollectionViewCell
        
        cell.configure(indexPath: indexPath)
        cell.layer.cornerRadius = 25
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                                         withReuseIdentifier: listOfIdentifiers.collectionHeader,
                                                                         for: indexPath) as! CustomCollectionReusableView
            header.configure(forSecton: indexPath)
            return header
        } else {
            return UICollectionReusableView()
        }
        
    }
}

