//
//  Constaints.swift
//  NetworkDemo
//
//  Created by Александр Евсеев on 25/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

struct listOfURLs {
    static let fetchCoursesURL = "https://swiftbook.ru/wp-content/uploads/api/api_courses"
    static let imageDownloadURL = "https://unsplash.com/photos/__U6tHlaapI/download?force=true"
    static let getRequestURL = "https://jsonplaceholder.typicode.com/users"
    static let postRequestURL = "https://jsonplaceholder.typicode.com/posts"
    static let download20Mb = "https://www.hq.nasa.gov/alsj/a17/A17_FlightPlan.pdf"
}

struct listOfSegues {
    static let segueImage = "showImageView"
    static let segueTable = "showTableView"
}

struct listOfIdentifiers {
    static let collectionCell = "CollectionCell"
    static let collectionHeader = "CollectionViewHeader"
    static let tableCell = "TableCell"
}
