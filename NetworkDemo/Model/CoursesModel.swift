//
//  CoursesModel.swift
//  NetworkDemo
//
//  Created by Александр Евсеев on 25/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

struct CoursesModel: Decodable {
    let id: Int?
    let name: String?
    let link: String?
    let imageUrl: String?
    let numberOfLessons: Int?
    let numberOfTests: Int?
    
    init? (jsonArray: [String : Any]) {
        let id = jsonArray["id"] as? Int
        let name = jsonArray["name"] as? String
        let link = jsonArray["link"] as? String
        let imageUrl = jsonArray["imageUrl"] as? String
        let numberOfLessons = jsonArray["number_of_lessons"] as? Int
        let numberOfTests = jsonArray["number_of_tests"] as? Int
        
        self.id = id
        self.name = name
        self.link = link
        self.imageUrl = imageUrl
        self.numberOfLessons = numberOfLessons
        self.numberOfTests = numberOfTests
    }
    
    static func getArray(from jsonArray: Any) -> [CoursesModel]? {
        guard let jsonArray = jsonArray as? Array<[String: Any]> else { return nil }
        
        var courses = [CoursesModel]()
        
        for field in jsonArray {
            if let course = CoursesModel(jsonArray: field) {
                courses.append(course)
            }
        }
        return courses
    }
}
