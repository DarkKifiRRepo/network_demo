//
//  AlamofireManager.swift
//  NetworkDemo
//
//  Created by Александр Евсеев on 26/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit
import Alamofire

class AlamofireManager {
    
    static var onProgress: ((Double)->())?
    static var completed: ((String)->())?
    
    static func sendRequest(with urlAddress: String) {
        
        guard let url = URL(string: urlAddress) else { return }
        AF.request(url, method: .get).validate().responseJSON { (responseJSON) in
            
            switch responseJSON.result {
            case .success(let data): print(data)
            case .failure(let error): print(error)
            }
        }
    }
    
    static func fetchCourses(with urlAddress: String, handler: @escaping ([CoursesModel]?)->()) {
        
        guard let url = URL(string: urlAddress) else { return }
        AF.request(url, method: .get).validate().responseJSON { (responseJSON) in
            
            switch responseJSON.result {
            case .success(let data):
                
                var courses = [CoursesModel]()
                courses = CoursesModel.getArray(from: data)!

                handler(courses)
            case .failure(let error): print(error)
            }
            
        }
    }
    
    static func downloadImage(with urlAddress: String, handler: @escaping (UIImage?)->() ) {
        guard let url = URL(string: urlAddress) else { return }
        
        AF.request(url).validate().downloadProgress { progress in
            self.onProgress?(progress.fractionCompleted)
            self.completed?(progress.localizedDescription!)
            }.response { response in
                guard let data = response.data, let image = UIImage(data: data) else { return }
                DispatchQueue.main.async {
                    handler(image)
                }
        }
    }
    
    static func postRequest(with urlAddress: String) {
        guard let url = URL(string: urlAddress) else { return }

        let userData: [String: Any] = [ "name": "Ervin Howell",
                                        "username": "Antonette",
                                        "website": "anastasia.net",
                                        "id": 3]

        AF.request(url, method: .post, parameters: userData).validate().responseJSON { responseJSON in
            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("Response status code: ", statusCode)
            
            switch responseJSON.result {
            case .success(let data):
                print(data)
            case .failure(let responseError):
                print(responseError)
            }
        }
    }
}
