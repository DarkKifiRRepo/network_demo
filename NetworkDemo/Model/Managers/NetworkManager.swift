//
//  NetworkManager.swift
//  NetworkDemo
//
//  Created by Александр Евсеев on 25/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

class NetworkManager {
    
    
    static func downloadImage(with urlAddress: String, handler: @escaping (UIImage?)->() ) {
        guard let url = URL(string: urlAddress) else { return }
        let session = URLSession.shared
        session.dataTask(with: url) { (data, responce, error) in
            if let data = data, let image = UIImage(data: data) {
                handler(image)
            } else {
                handler(nil)
            }
        }.resume()
    }
    
    static func getRequest(with urlAddress: String) {
        guard let url = URL(string: urlAddress) else { return }
        let session = URLSession.shared
        session.dataTask(with: url) { (data, responce, error) in
            guard let responce = responce,
                let data = data
                else { return }
            
            print(responce)
            print(data)
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
            } catch {
                print(error)
            }
        }.resume()
    }
    
    static func postRequest(with urlAddress: String) {
        guard  let url = URL(string: urlAddress) else { return }
        
        let dummyPost = ["getRequest" : "testing", "postRequest" : "testing"]
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        guard let httpBody = try? JSONSerialization.data(withJSONObject: dummyPost, options: []) else { return }
        request.httpBody = httpBody
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            guard let response = response,
                let data = data
                else { return }
            print(response)
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
            } catch {
                print(error)
            }
        }.resume()
    }
    
    static func fetchCoursesData(with urlAddress: String, handler: @escaping ([CoursesModel]?)->() ) {
        guard let url = URL(string: urlAddress) else { return }
        
        let session = URLSession.shared
        session.dataTask(with: url) { (data, responce, error) in
            guard let data = data else { return }
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            
            do {
                let result = try decoder.decode([CoursesModel].self, from: data)
                handler(result)
            } catch {
                print(error)
            }
        }.resume()
    }
}
