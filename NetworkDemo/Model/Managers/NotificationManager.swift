//
//  NotificationManager.swift
//  NetworkDemo
//
//  Created by Александр Евсеев on 26/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UserNotifications

class NotificationManager {
    static func registerForNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (_, _) in }
    }
    
    static func postNotification(filePath: String) {
        let content = UNMutableNotificationContent()
        content.title = "Download complete"
        content.body = "Your background transfer has completed. File path: \(filePath)"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
        let request = UNNotificationRequest(identifier: "Transfere complete", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}
