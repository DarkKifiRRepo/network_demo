//
//  MenuItems.swift
//  NetworkDemo
//
//  Created by Александр Евсеев on 25/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

enum SectionCollectionType: String, CaseIterable {
    case httpsMethods = "HTTPS Requests"
    case alamofireMethods = "Requests with AlamoFire"
    case fireBaseMethods = "Firebase Methods"
    case comingSoon = "Coming Soon"
    
    var items: [MenuItems] {
        return MenuItems.allCases
    }
}

enum MenuItems: String, CaseIterable {
    case getRequest = "GET Request"
    case postRequest = "POST Request"
    case downloadImage = "Download Image"
    case coursesFetch = "Fetch courses data"
    case downloadFile = "Download file on device"
    
}
