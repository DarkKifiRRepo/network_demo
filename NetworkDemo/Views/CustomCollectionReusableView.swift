//
//  CustomCollectionReusableView.swift
//  NetworkDemo
//
//  Created by Александр Евсеев on 26/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

class CustomCollectionReusableView: UICollectionReusableView {
    @IBOutlet private weak var titleTypeLabel: UILabel!
    
    func configure(forSecton indexPath: IndexPath) {
        titleTypeLabel.text = SectionCollectionType.allCases[indexPath.section].rawValue
    }
}
