//
//  CustomCollectionViewCell.swift
//  NetworkDemo
//
//  Created by Александр Евсеев on 25/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var menuLabel: UILabel!
    var sectionType: SectionCollectionType!
    var menuItem: MenuItems!
    
    func configure(indexPath: IndexPath) {
        sectionType = SectionCollectionType.allCases[indexPath.section]
        menuItem = MenuItems.allCases[indexPath.row]
        menuLabel.text = menuItem.rawValue
    }
}
