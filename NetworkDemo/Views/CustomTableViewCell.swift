//
//  CustomTableViewCell.swift
//  NetworkDemo
//
//  Created by Александр Евсеев on 25/04/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var imageLogoView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var countLessonsLabel: UILabel!
    @IBOutlet private weak var countTestLabel: UILabel!
    
    func configureCell(with course: CoursesModel) {
        titleLabel.text = course.name
        countLessonsLabel.text = "Количество уроков: " + String(course.numberOfLessons!)
        countTestLabel.text = "Количество тестов: " + String(course.numberOfTests!)
    }
}
